import Time from "./cdb/Time.js";
import Estadio from "./cdb/Estadio.js";
import Jogo from "./cdb/Jogo.js";
import Arbitro from "./cdb/Arbitro.js";

let time1 = new Time (1, "Volta Redonda", "Rio de Janeiro");
console.log(time1);
let time2 = new Time (2, "Bahia", "Bahia");
console.log(time2)
let time3 = new Time (3, "Internacional" , "Rio Grande do Sul");
console.log(time3);
let time4 = new Time (4, "CSA", "Alagoas");
console.log(time4);
let time5 = new Time (5, "São Paulo", "São Paulo");
console.log(time5);
let time6 = new Time (6 , "Ituano", "São Paulo");
console.log(time6);
let time7 = new Time (7, "Coritiba", "Paraná");
console.log(time7);
let time8 = new Time (8, "Sport Recife", "Pernambuco");
console.log(time8);
let time9 = new Time (9, "Fluminense", "Rio de Janeiro");
console.log(time9);
let time10 = new Time (10, "Paysandu", "Pará");
console.log(time10);
let time11 = new Time (11, "Remo", "Pará");
console.log(time11);
let time12 = new Time (12, "Corinthians", "São Paulo")
console.log(time12);
let time13 = new Time (13, "Ypiranga", "Rio Grande do Sul")
console.log(time13);
let time14 = new Time (14, "Botafogo", "Rio de Janeiro")
console.log(time14);
let time15 = new Time (15,"", "Maringá", "Paraná")
console.log(time15);
let time16 = new Time (16, "Flamengo", "Rio de Janeiro")
console.log(time16);
let time17 = new Time (17, "Botafogo SP", "São Paulo")
console.log(time17);
let time18 = new Time (18, "Santos", "São Paulo")
console.log(time18);
let time19 = new Time (19, "Fortaleza", "Ceará")
console.log(time19);
let time20 = new Time (20, "Águia de Marabá", "Pará")
console.log(time20);
let time21 = new Time (21, "Nova Iguaçu", "Rio de Janeiro")
console.log(time21);
let time22 = new Time (22, "America-MG", "Minas Gerais")
console.log(time21);
let time23 = new Time (23, "CRB", "Alagoas")
console.log(time22);
let time24 = new Time (24, "Athletico-PR", "Paraná")
console.log(time24);
let time25 = new Time (25, "Palmeiras", "São Paulo")
console.log(time25);
let time26 = new Time (26, "Tombense", "Minas Gerais")
console.log(time26);
let time27 = new Time (27, "Atlético-MG", "Minas Gerais")
console.log(time27);
let time28 = new Time (28, "Brasil de Pelotas", "Rio Grande do Sul")
console.log(time28);
let time29 = new Time (29, "Náutico", "Pernambuco")
console.log(time29);
let time30 = new Time (30, "Cruzeiro", "Minas Gerais")
console.log(time30);
let time31 = new Time (31, "ABC", "Rio Grande do Norte")
console.log(time31);
let time32 = new Time (32, "Grêmio", "Rio Grande do Sul")
console.log(time32);

let jogo1 = new Jogo ("1", "Estádio Raulino de Oliveira", "16h30" , "11 de abril de 2023", "90 minutos e acréscimos", "Volta Redonda" , "Bahia" , "1 a 2" , "8" , "Jogo 1 de 2" , "Luiz Flávio de Oliveira" );
console.log(jogo1);

let jogo2 = new Jogo ("2", "Estádio Beira-Rio", "20h", "11 de abril de 2023", "90 minutos e acréscimos", "Internacional", "CSA", "2 a 1", "4", "Jogo 1 de 2", "Yuri Elino Ferreira da Cruz");
console.log(jogo2);

let jogo3 = new Jogo("3", "Estádio do Morumbi", "21h30", "11 de abril de 2023", "90 minutos e acréscimos" , "São Paulo", "Ituano", "0 a 0", "3", "Jogo 1 de 2", "Caio Max Augusto Vieira");
console.log(jogo3);

let jogo4 = new Jogo("4", "Couto Pereira", "19h", "12 de abril de 2023", "90 minutos e acréscimos", "Coritiba", "Sport Recife", "3 a 3", "6", "Jogo 1 de 2", "Leandro Pedro Vuaden");
console.log(jogo4);

let jogo5 = new Jogo ("5", "Estádio do Maracanã", "19h30", "12 de abril de 2023", "90 minutos e acréscimos", "Fluminense" , "Paysandu", "3 a 0", "2" , "Jogo 1 de 2" , "Anderson Daronco");
console.log(jogo5);

let jogo6 = new Jogo ("6", "Estádio Mangueirão", "21h30", "12 de abril de 2023", "90 minutos e acréscimos" , "Remo" , "Corinthians", "2 a 0" , "3" , "Jogo 1 de 2" , "Ramon Abatti Abel");
console.log(jogo6);

let jogo7 = new Jogo ("7", "Colosso da Lagoa", "20h30", "12 de abril de 2023", "90 minutos e acréscimos", "Ypiranga", "Botafogo", "0 a 2", "5", "Jogo 1 de 2", "Paulo Cesar Zanovelli da Silva");
console.log(jogo7);

let jogo8 = new Jogo ("8", "Estádio Willie Davids", "20h" , "13 de abril de 2023", "90 minutos e acréscimos", "Maringá", "Flamengo", "2 a 0", "3" , "Jogo 1 de 2" , "Flávio Rodrigues de Souza");
console.log(jogo8);

let jogo9 = new Jogo("9", "Estádio Santa Cruz" , "19h" , "11 de abril de 2023" , "90 minutos e acréscimos" , "Botafogo SP" ,  "Santos" , "0 a 2" , "7" , "Jogo 1 de 2" , "Bráulio da Silva Machado");
console.log(jogo9);
                     
let jogo10= new Jogo("10", "Arena Castelão", "21h30", "11 de abril de 2023", "90 minutos e acréscimos", "Fortaleza", "Águia de Marabá", "6 a 1", "1", "Jogo 1 de 2", "Luiz Cesar de Oliveira Magalhães");
console.log(jogo10);

let jogo11 = new Jogo("11" , "Estádio General Raulino de Oliveira" , "16h30" , "12 de abril de 2023" , "90 minutos e acréscimos", "Nova Iguaçu" , "América-MG" , "1 a 2" , "10" , "Jogo 1 de 2" , "Diego Pombo Lopez");
console.log(jogo11);

let jogo12 = new Jogo("12", "Estádio Rei Pelé", "19h30", "12 de abril de 2023", "90 minutos e acréscimos", "CRB", "athletico-PR", "1 a 0", "8", "Jogo 1 de 2", "Leandro Pedro Vuaden");
console.log(jogo12);

let jogo13 = new Jogo("13","Allianz Parque", "20h", "12 de abril de 2023", "90 minutos e acréscimos", "Palmeiras", "Tombense", "4 a 2", "4", "Jogo 1 de 2", "Bruno Arleu de Araujo");
console.log(jogo13);

let jogo14 = new Jogo("14", "Estádio Mineirão" , "21h30" , "12 de abril de 2023" , "90 minutos e acréscimos" , "Atlético-MG" , "  Brasil de Pelotas", "2 a 1" , "8" , "Jogo 1 de 2" , "Edina Alves Batista");
console.log(jogo14);

let jogo15 = new Jogo("15", "Estádio Eládio de Barros Carvalho", "19h", "13 de abril de 2023", "90 minutos e acréscimos", "Naútico", "Cruzeiro", "1 a 0", "7", "Jogo 1 de 2", "Marcelo de Lima Henrique");
console.log(jogo15);

let jogo16 = new Jogo("16", "Estádio Maria Lamas Farache", "21h30", "13 de abril de 2023", "90 minutos e acréscimos", "ABC" , "Grêmio", "0 a 2" , "0" , "Jogo 1 de 2", "Raphael Claus");
console.log(jogo16);

let estadio1 = new Estadio("1", "Estádio Raulino de Oliveira", "Rio de Janeiro", "20 mil torcedores");
console.log(estadio1);

let estadio2 = new Estadio("2", "Estádio Beira-Rio", "Rio Grande do Sul", "50 mil torcedores");
console.log(estadio2);

let estadio3 = new Estadio("3" , "Estádio do Morumbi" , "São Paulo", "66 mil torcedores");
console.log(estadio3);

let estadio4 = new Estadio("4", "Couto Pereira", "Paraná", "40 mil torcedores");
console.log(estadio4);

let estadio5 = new Estadio("5", "Estádio do Maracanã" , "Rio de Janeiro", "78 mil torcedores");
console.log(estadio5);

let estadio6 = new Estadio("6", "Mangueirão", "Pará", "53 mil torcedores");
console.log(estadio6);

let estadio7 = new Estadio("7", "Colosso da Lagoa", "Rio Grande do Sul" , "22 mil torcedores");
console.log(estadio7);

let estadio8 = new Estadio("8", "Estádio Willie Davids", "Paraná", "15 mil torcedores");
console.log(estadio8);

let estadio9 = new Estadio("9", "Santa Cruz", "São Paulo", "30 mil torcedores");
console.log(estadio9);

let estadio10 = new Estadio("10", "Arena Castelão", "Ceará" , "64 mil torcedores");
console.log(estadio10);

let estadio11 = new Estadio ("11", "Estádio Municipal General Raulino de Oliveira", "Rio de janeiro", "20 mil torcedores");
console.log(estadio11);

let estadio12 = new Estadio("12", "Estádio Rei Pelé" , "Alagoas", "20 mil torcedores");
console.log(estadio12);

let estadio13 = new Estadio("13","Allianz Parque" , "São Paulo", "43 mil torcedores");
console.log(estadio13);

let estadio14 = new Estadio("14", "Estádio Mineirão", "Minas Gerais", "60 mil torcedores");
console.log(estadio14);

let estadio15 = new Estadio("15", "Estádio Eládio de Barros Carvalho", "Pernambuco", "20 mil torcedores");
console.log(estadio15);

let estadio16 = new Estadio("16", "Estádio Maria Lamas Farache", "Rio Grande do Norte", "18 mil torcedores");
console.log(estadio16);

let arbitro1 = new Arbitro("1", "Luiz Flávio de Oliveira", "Juiz");
console.log(arbitro1);

let arbitro2 = new Arbitro("2", "Yuri Elino Ferreira da Cruz", "Juiz");
console.log(arbitro2);

let arbitro3 = new Arbitro("3", "Caio Max Augusto Vieira", "Juiz");
console.log(arbitro3);

let arbitro4 = new Arbitro("4", "Leandro Pedro Vuaden", "Juiz");
console.log(arbitro4);

let arbitro5 = new Arbitro("5", "Anderson Daronco", "Juiz");
console.log(arbitro5);

let arbitro6 = new Arbitro("6", "Ramon Abatti Abel", "Juiz");
console.log(arbitro6);

let arbitro7 = new Arbitro("7","Paulo Cesar Zanovelli da Silva" , "Juiz");
console.log(arbitro7);

let arbitro8 = new Arbitro("8", "Flávio Rodrigues de Souza", "Juiz");
console.log(arbitro8);

let arbitro9 = new Arbitro("9","Bráulio da Silva Machado" , "Juiz");
console.log(arbitro9);

let arbitro10 = new Arbitro("10", "Luiz Cesar de Oliveira Magalhães", "Juiz");
console.log(arbitro10);

let arbitro11 = new Arbitro("11", "Diego Pombo Lopez", "Juiz");
console.log(arbitro11);

let arbitro12 = new Arbitro("12", "Leandro Pedro Vuaden", "Juiz");
console.log(arbitro12);

let arbitro13 = new Arbitro("13", "Leandro Pedro Vuaden" , "Juiz");
console.log(arbitro13);

let arbitro14 = new Arbitro("14", "Edina Alves Batista", "Juiz");
console.log(arbitro14);

let arbitro15 = new Arbitro("15", "Marcelo de Lima Henrique", "Juiz");
console.log(arbitro15);

let arbitro16 = new Arbitro("16", "Raphael Claus", "Juiz");
console.log(arbitro16);





