export default class Estadio {
  constructor(id, nome, localizacao , capacidadeDeTorcedores){
    this.id = id;
    this.nome = nome;
    this.localizacao = localizacao;
    this.capacidadeDeTorcedores = capacidadeDeTorcedores;
  }
}
